//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

protocol IssueDataStore {
  func saveIssue(isse: Issue) -> (savedIssueOrNil: Issue?, errorOrNil: NSError?)
  func bookmarkIssue(issue: Issue)
}