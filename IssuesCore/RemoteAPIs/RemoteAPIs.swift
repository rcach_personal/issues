//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

protocol IssueRemoteAPI {
  func createIssue(data: CreateIssueData, completionHandler: (issueOrNil: Issue?, errorOrNil: NSError?)->())
}