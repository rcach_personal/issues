//
//  IssuesCore.h
//  IssuesCore
//
//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IssuesCore.
FOUNDATION_EXPORT double IssuesCoreVersionNumber;

//! Project version string for IssuesCore.
FOUNDATION_EXPORT const unsigned char IssuesCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IssuesCore/PublicHeader.h>


