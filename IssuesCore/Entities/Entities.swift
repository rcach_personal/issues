//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

struct CreateIssueData {
  let title: String
  let body: String
}

struct Issue {
  let title: String
  let body: String
}
