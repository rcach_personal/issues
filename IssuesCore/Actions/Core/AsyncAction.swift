//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

class AsyncAction: NSOperation {
  private var _executing = false
  private var _finished = false
  
  override private(set) var executing: Bool {
    get {
      return _executing
    }
    set {
      willChangeValueForKey("isExecuting")
      _executing = newValue
      didChangeValueForKey("isExecuting")
    }
  }
  
  override private(set) var finished: Bool {
    get {
      return _finished
    }
    set {
      willChangeValueForKey("isFinished")
      _finished = newValue
      didChangeValueForKey("isFinished")
    }
  }
  
  override var asynchronous: Bool {
    return true
  }
  
  override func start() {
    if cancelled {
      finished = true
      return
    }
    
    executing = true
    autoreleasepool {
      self.main()
    }
  }
  
  override func main() {
    autoreleasepool {
      self.runAsync()
    }
  }
  
  func runAsync() {
    // Abstract.
  }
  
  func finishExecutingOperation() {
    executing = false
    finished = true
  }
}
