//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

class Action: NSOperation {
  override func main() {
    if cancelled {
      return
    }
    autoreleasepool {
      self.run()
    }
  }
  
  func run() {
    // Abstract.
  }
}