//  Created by Rene Cacheaux on 1/21/15.
//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

class CreateIssueAction: AsyncAction {
  // Dependencies
  let issueRemoteAPI: IssueRemoteAPI
  let issueDataStore: IssueDataStore
  
  // Input
  let data: CreateIssueData
  
  // Output
  var createdIssueOrNil: Issue?
  var errorOrNil: NSError?
  
  init(issueRemoteAPI: IssueRemoteAPI, issueDataStore: IssueDataStore, data: CreateIssueData) {
    self.issueRemoteAPI = issueRemoteAPI
    self.issueDataStore = issueDataStore
    self.data = data
  }
  
  override func main() {
    issueRemoteAPI.createIssue(data) { [weak self] issueOrNil, errorOrNil in
      if let strongSelf = self {
        if let issue = issueOrNil {
          let saveResult = strongSelf.issueDataStore.saveIssue(issue)
          strongSelf.createdIssueOrNil = saveResult.savedIssueOrNil // TODO: Figure out how to handle errors here.
        }
        strongSelf.errorOrNil = errorOrNil
        strongSelf.finishExecutingOperation()
      }
    }
  }
}
